# Convert DNA to RNA

## Spec
This program should take a string from the user that is made of any combination of `G`, `C`, `T`, or `A`. This program should convert any `G` to a `C`, `C` to `G`, `T` to `A`, and `A` to `U`, and then output the converted string.

If I was to input `GGCCTTAA`, `CCFFAAUU`.

If the user inputs any invalid characters, then the program should say `Enter only G, C, T or A!` and exit.

## Hints
- Python has a useful function that will convert a string to a list of characters: `list()`
    ```python
    letters = 'asdfadsf'
    chars = list(letters)
    ```
