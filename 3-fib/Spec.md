# Fibonacci Sequence

## Spec
This program should compute the first 20 numbers in the Fibonacci Sequence, and for each number, say if it is prime.

The program also should **NOT** use for loops, and should instead use recursion to compute values.

Example output:
```
#1       0       not prime
#2       1       not prime
#3       1       not prime
#4       2       prime
#5       3       prime
#6       5       prime
#7       8       not prime
#8       13      prime
#9       21      not prime
#10      34      not prime
#11      55      not prime
#12      89      prime
#13      144     not prime
#14      233     prime
#15      377     not prime
#16      610     not prime
#17      987     not prime
#18      1597    prime
#19      2584    not prime
#20      4181    not prime
```

## Hints
- Every recursive function needs a "base condition", or the condition for which it no longer recurses.
- Python template strings make output much easier
- Python can use a parameter default to make it so you don't need a helper function
