# Hello Person

## Spec
This program should ask a person for their name (`Please enter your name: `) and once the user enters their name, it should output `Hi there NAME! Nice day, eh?` so that if you entered `Jo` it would say `Hi there Jo! Nice day, eh?`.

**Note:** For this challenge we will use Python. This is because it is easier to do some simple operations for this challenge.

## Hints
- Getting input is as easy as `name = input('Please enter your name: ')`

Here is an example piece of Python code to help you understand the syntax:

```python
print('Hi there!')
def a_function(one, two):
  return one + two
taco = input('Tell me a number!')
print(a_function(taco, 30))
```
